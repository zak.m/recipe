package recipe.com;

import java.util.ArrayList;

public class Recipe {
	
	private String author, recipeName;
	ArrayList<String> ingredients=new ArrayList<String>();
	ArrayList<String> steps=new ArrayList<String>();
	int noOfSteps;
	
	
	public int getNoOfSteps() {
		return noOfSteps;
	}
	public void setNoOfSteps(int noOfSteps) {
		this.noOfSteps = noOfSteps;
	}
	public String getAuthor() {
		return author;
	}
	public void setAuthor(String author) {
		this.author = author;
	}
	public String getRecipeName() {
		return recipeName;
	}
	public void setRecipeName(String recipeName) {
		this.recipeName = recipeName;
	}
	public ArrayList<String> getIngredients() {
		return ingredients;
	}
	public ArrayList<String> getSteps() {
		return steps;
	}
	
	public void addIngredients(String ing)
	{
		ingredients.add(ing);
	}	
	public void addSteps(String stp)
	{
		steps.add(stp);
	}
	
	
	
	
	
	
	

}
