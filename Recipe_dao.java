package recipe.com;

import java.util.ArrayList;
import java.util.Scanner;

public class Recipe_dao {

	public static ArrayList<Recipe> recipeStorage = new ArrayList<Recipe>();

	public static ArrayList<Recipe> getRecipeStorage() {
		return recipeStorage;

	}

	public void addrecipe(Scanner sc) {
		/*
		 * addrecipe method Asks for the author name Asks for the ingredients separated
		 * bya comma Adds the ingredients to the rc object Aks ofr the number of steps
		 * Asks for each step individually
		 */
		System.out.print("Please enter recipe name:");
		Recipe rc = new Recipe();
		rc.setRecipeName(sc.nextLine());
		System.out.print("Please enter author name:");
		rc.setAuthor(sc.nextLine());
		System.out.print("Enter the ingredients separted by , (comma):");
		String ingredients = sc.nextLine();
		String any[] = ingredients.split(",");
		for (String xyz : any) {
			rc.addIngredients(xyz);
		}
		System.out.print("Please enter the numebr of steps in your recipe:");
		rc.setNoOfSteps(sc.nextInt());
		if (sc.hasNextLine()) {
			sc.nextLine();
		}
		for (int i = 0; i < rc.getNoOfSteps(); i++) {
			System.out.println("Enter " + (i + 1) + " Step");
			rc.addSteps(sc.nextLine());
		}
		recipeStorage.add(rc);
	}

	public void display() {
		/*
		 * display the dish prints the the recipes entered in the programmed and their
		 * number of steps gets it via the recipeStorage array list
		 */
		System.out.println("Select the dish");
		int i = 0;
		for (; i < recipeStorage.size(); i++) {
			System.out.println((i + 1) + " - " + recipeStorage.get(i).getRecipeName() + " number of steps -> "
					+ recipeStorage.get(i).getNoOfSteps());

		}

		System.out.println("Enter your choice");
		Scanner sc = new Scanner(System.in);

		int c = sc.nextInt();

		if (sc.hasNextLine()) {
			sc.nextLine();

		}

		if (c >= 1 && c <= recipeStorage.size()) {
			displaybyIndex(c - 1);

		}

		else {
			System.out.println("Value is not valid");

		}

	}

	/**
	 * variable stores the data from recipe storage for loop gets the ingredients
	 * from rc, stored in variable and the variable is then printed for loop gets
	 * the steps from rc, stored in variable and the variable is then printed
	 */
	public void displaybyIndex(int index) {
		Recipe rc = recipeStorage.get(index);
		System.out.println("\tRecipe Name:" + rc.getRecipeName());
		System.out.println("\tAuthor: " + rc.getAuthor());
		System.out.println("\tNoOfSteps: " + rc.getNoOfSteps());
		System.out.print("\tIngredients: ");

		for (String ing : rc.getIngredients()) {
			System.out.print(ing);

		}
		System.out.println("\n\tSteps:");

		for (String ing : rc.getSteps()) {
			System.out.println("\t-> " + ing.trim());

		}

	}

	public void displaybyrecipe(Recipe rc) {
		// Recipe rc = recipeStorage.get(index);

		System.out.println("\tRecipe Name:" + rc.getRecipeName());
		System.out.println("\tAuthor: " + rc.getAuthor());
		System.out.println("\tNoOfSteps: " + rc.getNoOfSteps());
		System.out.print("\tIngredients: ");

		for (String ing : rc.getIngredients()) {
			System.out.print(ing);

		}
		System.out.println("\n\tSteps:");

		for (String ing : rc.getSteps()) {
			System.out.println("\t-> " + ing.trim());

		}

	}

	/**
	 * input stored in variable the data from variable is stored in the array list
	 * removing , the for loop stores the array list into xyz Variable stores data
	 * of ingredients from recipeStorage for each iteration if statement checks if
	 * the ingredients entered by user already exist
	 */
	public void displaybyingredients()

	{
		Scanner sc = new Scanner(System.in);
		System.out.print("Enter the ingredients separted by , (comma):");
		String ingredients = sc.nextLine();
		String any[] = ingredients.split(",");
		ArrayList<Recipe> temp = new ArrayList<Recipe>();
		int newcount = 0;
		for (int i = 0; i < recipeStorage.size(); i++) {
			int count = 0;

			for (String xyz : any) {
				for (String ing : recipeStorage.get(i).getIngredients()) {
					xyz = xyz.trim();
					ing = ing.trim();

					if (xyz.equalsIgnoreCase(ing)) {
						count++;

					}

				}

			}

			if (count == any.length) {
				temp.add(recipeStorage.get(i));
				System.out.println((newcount + 1) + " - " + temp.get(newcount).getRecipeName() + " number of steps -> "
						+ temp.get(newcount).getNoOfSteps());
				newcount++;
			}
		}
		if (temp.isEmpty()) {
			System.out.println("No matching ingredients found");
		} else {

			System.out.println("Enter your choice");

			int c = sc.nextInt();

			if (sc.hasNextLine()) {
				sc.nextLine();

			}

			if (c >= 1 && c <= temp.size()) {
				// displaybyIndex(c - 1);
				displaybyrecipe(temp.get(c - 1));

			}

			else {
				System.out.println("Value is not valid");

			}
		}

	}

	/**
	 * author name is collected if the author entered by user matches the one in the
	 * system displays all the recipes by index of the author including the recipe
	 * name and number of steps.
	 */
	public void searchbyauthor()

	{
		Scanner sc = new Scanner(System.in);
		System.out.print("Please enter author name:");
		String author_name = sc.nextLine();
		ArrayList<Recipe> temp = new ArrayList<Recipe>();
		int newcount = 0;
		for (int i = 0; i < recipeStorage.size(); i++) {
			Recipe rc = recipeStorage.get(i);
			if (rc.getAuthor().equalsIgnoreCase(author_name)) {
				temp.add(rc);
				System.out.println((newcount + 1) + " - " + rc.getRecipeName() + " number of steps -> " + rc.getNoOfSteps());
				newcount++;
			}
		}
		if (temp.isEmpty()) {
			System.out.println("No matching ingredients found");
		} else {

			System.out.println("Enter your choice");

			int c = sc.nextInt();

			if (sc.hasNextLine()) {
				sc.nextLine();

			}

			if (c >= 1 && c <= temp.size()) {
				// displaybyIndex(c - 1);
				displaybyrecipe(temp.get(c - 1));

			}

			else {
				System.out.println("Value is not valid");

			}
		}

	}
}
