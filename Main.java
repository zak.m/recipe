package recipe.com;

import java.util.Scanner;

public class Main {

	public static void main(String[] args) {
		System.out.println("Welcome to your Recipe Management System!");
		Scanner sc = new Scanner(System.in);
		
		Recipe_dao rd = new Recipe_dao(); 
		
		
		while(true) {
			System.out.println("-----------------------------------------------------------------------------------------------------------");
			System.out.println("Press \n1 to view the recipe list \n2 to add a recipe \n3 to search by ingredient \n4 to display by author \n5 to exit");
			int choice = sc.nextInt();
			
			if(sc.hasNextLine()) {
				sc.nextLine();
			}
			switch(choice) {
			case 1:
				System.out.println("Below is the recipe list is displayed.");
				rd.display();
				break;
			case 2:
				System.out.println("Add the recipe of your choice!");
				rd.addrecipe(sc);
				break;
			case 3:
				System.out.println("Please list the ingredients you want to search by");
				rd.displaybyingredients();
				break;
			case 4:
				System.out.println("Search by author");
				rd.searchbyauthor();
				break;
			case 5:
				System.exit(0);
				break;
			default:
				System.out.println("Not applicable.");
				
			
				
			}
			
			
		}
			
				
	}
}
